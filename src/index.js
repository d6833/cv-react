// Import de la library
import React from "react";
import ReactDom from "react-dom";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Home from "./components/Pages/Home";
import Contact from "./components/Pages/Contact";// import de la constante Contact qui se situe dans ./Page/Contact.js
import Competence from "./components/Pages/Competence";
import Portofolio from "./components/Pages/Portofolio";
import Profil from "./components/Pages/Profil";
import Formation from "./components/Pages/Formation";
import "./Stylesheet/StyleSideNav.scss";
import App from "./App";
// Import de la CSS du Sidenav


// Fonction qui appelle le routeur et un switch qui fait la redirection vers les bonnes pages quand l'on tape dans l'url
const Root = () => {
    return (
        <main>
            <div className="container-home">
                <BrowserRouter>
                    <Switch>
                        <Route path="/" exact component={Home}/>
                        <Route path="/contact" component={Contact}/>
                        <Route path="/competence" component={Competence}/>
                        <Route path="/portofolio" component={Portofolio}/>
                        <Route path="/profil" component={Profil}/>
                        <Route path="/formation" component={Formation}/>
                    </Switch>
                    <App/>
                </BrowserRouter>
            </div>
        </main>



    );
};
// Affichage de la fonctionnalité sur le  site
ReactDom.render(
    <Root/>,
    document.querySelector("#root"),

);