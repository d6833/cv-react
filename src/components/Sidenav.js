import React, {useState} from 'react';
import img from "./img/dorian-avatar.png";
import {NavLink} from "react-router-dom";


const Sidenav = () => {

    // const [navOpen, setNavOpen] = useState(true)

    return (

        <header>

                {/* Menu de Navigation */}
            {/*{navOpen &&  /!* activation du bouton burger *!/*/
            <nav  className="sidenav">

                {/* Avatar */}
                    <div className="image">
                        <img className="avatar" src={img} alt="mon avatar"/>
                    </div>

                {/*<button*/}
                {/*    className="button-burger"*/}
                {/*    type="button"*/}
                {/*    id="js-button-burger"*/}
                {/*    onClick={() => setNavOpen(!navOpen)}*/}
                {/*>*/}
                {/*    <span/>*/}
                {/*    <span/>*/}
                {/*    <span/>*/}
                {/*</button>*/}

                    {/* Menu de navigation */}
                    <ul className="list-sidenav">

                        {/*/!* Home *!/*/}
                        {/*<li className="sidenav-home">*/}
                        {/*    <NavLink exact to="/" >*/}
                        {/*        Accueil*/}
                        {/*    </NavLink>*/}
                        {/*</li>*/}

                        {/* Profil */}
                        <li className="sidenav-home">
                            <NavLink exact to="/profil" >
                                Profil
                            </NavLink>
                        </li>

                        {/* Compétence */}
                        <li className="sidenav-competence">
                            <NavLink exact to="/competence" >
                                Compétence
                            </NavLink>
                        </li>

                        {/* Formation et experience */}
                        <li className="sidenav-experience">
                            <NavLink exact to="/formation">
                                Formation et experience
                            </NavLink>
                        </li>

                        {/*/!* Portofolio *!/*/}
                        {/*<li className="sidenav-portofolio">*/}
                        {/*    <NavLink exact to="/portofolio">*/}
                        {/*        Portofolio*/}
                        {/*    </NavLink>*/}
                        {/*</li>*/}

                    {/*    /!* Contact *!/*/}
                    {/*    <li className="sidenav-contact">*/}
                    {/*        <NavLink exact to="/contact">*/}
                    {/*            Contact*/}
                    {/*        </NavLink>*/}
                    {/*    </li>*/}

                    </ul>

                    {/*  3 Projet  */}
                    <ul className="project-list">
                        <li className="project-1">Projet 1</li>
                        <li className="project-2">Projet 2</li>
                        <li className="project-3">Projet 3</li>
                    </ul>

                    <span>copyright gna gna</span>
                </nav>}

                                {/*/!* Linkedin *!/*/}
                {/*//                         /!*<div className="socialNetwork">*!/*/}
                {/*//                         /!*    <ul>*!/*/}
                {/*//                         /!*        <li>*!/*/}
                {/*//                         /!*            <a href="https://www.google.com" target="_blank" rel="noopener noreferrer">*!/*/}
                {/*//                         /!*                <i className="fab fa-Linkedin"> Lien du linkedin</i>*!/*/}
                {/*//                         /!*            </a>*!/*/}
                {/*//                         /!*        </li>*!/*/}
                {/*//                         /!*    </ul>*!/*/}
                {/*//                         /!*</div>*!/*/}
                                {/*/!* GitLab *!/*/}
                {/*//                         /!*<div className="socialNetwork">*!/*/}
                {/*//                         /!*    <ul>*!/*/}
                {/*//                         /!*        <li>*!/*/}
                {/*//                         /!*            <a href="https://www.google.com" target="_blank" rel="noopener noreferrer">*!/*/}
                {/*//                         /!*                <i className="fab fa-Linkedin"> Lien du Gitlab</i>*!/*/}
                {/*//                         /!*            </a>*!/*/}
                {/*//                         /!*        </li>*!/*/}
                {/*//                         /!*    </ul>*!/*/}
                {/*//                         /!*</div>*!/*/}
        </header>
    );
};

export default Sidenav;
