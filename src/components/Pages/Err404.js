import React from 'react';

const err404 = () => {
    return (
        <div>
            Il n'y a rien ici
        </div>
    );
};

export default err404;