import React from 'react';
import "../Sidenav"


const Contact = () => {


    return (
        <div className="right-container">
            <form method="post" className="contact-form">

                <div className="div-form-firstname">
                    <label htmlFor="firstname"></label>
                    <input className="input-firstname" type="text" name="firstname" placeholder="entrer votre nom" required/>
                </div>

                <div className="div-form-lastname">
                    <label htmlFor="lastname"></label>
                    <input className="input-lastname" type="text" name="lastname" placeholder="entrer votre prénom" required/>
                </div>

                <div className="div-form-email">
                    <label htmlFor="email"></label>
                    <input className="input-name" type="email" name="email" placeholder="entrer votre email" required/>
                </div>

                <div className="submit">
                    <button type="submit">Envoyer</button>
                </div>

            </form>
        </div>
    );
};

export default Contact;