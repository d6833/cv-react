import React from 'react';
import "../Sidenav"
import pdf from "../img/CV Dorian Soudan.pdf";


const Profil = () => {

    return (
        <div className="right-container">
            {/* Div qui contient le nom, le prénom et le travail */}
            <div className="name-work">
                <h1>Developpeur Fullstack</h1>
                <h2>Soudan Dorian</h2>
            </div>

            <div className="home-text">
                <p>
                    Actuellement en formation à l'idem au Soler.
                    Je recherche une entreprise pour effectuer un stage du 13 septembre 2021 au 16 novembre 2021.
                    <br/><br/>
                    Développeur passionnée d'informatique.
                    Je souhaite me professionnaliser dans la conception et le développement web et d'application.
                </p>
            </div>

            {/* div qui contient la possibilité de télécharger le pdf papier */}
            <div className="pdf-dl">
                <a href={pdf} target="_blank">Télécharger le CV ici</a>
            </div>
        </div>
    );
};

export default Profil;