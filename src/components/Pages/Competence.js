import React from 'react';
import "../Sidenav"

import { AiFillHtml5 } from "react-icons/ai";
import { FaCss3Alt } from "react-icons/fa";
import { FaJsSquare } from "react-icons/fa";
import { SiTypescript } from "react-icons/si";
import { SiPhp } from "react-icons/si";
import { SiMysql } from "react-icons/si";

import { FaSymfony } from "react-icons/fa";
import { FaWordpress } from "react-icons/fa";
import { FaReact } from "react-icons/fa";
import { FaAngular } from "react-icons/fa";
import { SiApachecordova } from "react-icons/si";
import { SiLaravel } from "react-icons/si";
import { FaCuttlefish } from "react-icons/fa";

import { BiPlanet } from "react-icons/bi";
import { FaDocker } from "react-icons/fa";
import { AiOutlineGitlab } from "react-icons/ai";
import { AiFillGithub } from "react-icons/ai";
import { SiHeroku } from "react-icons/si";
import { SiDiscord } from "react-icons/si";
import { SiUbuntu } from "react-icons/si";
import { SiArduino } from "react-icons/si";






const Competence = () => {
    return (

<div className="right-container">

    {/* Div qui contient le nom, le prénom et le travail */}
    <div className="name-work">
        <h1>Compétence</h1>
    </div>

    <div className="competence-container-w">
        <div className="language">
            <h2>Language :</h2>
            <ul className="language-list">
                <li className="item-html"> <AiFillHtml5 /> Html 5 : utilisé</li>
                <li className="item-css"> <FaCss3Alt /> CSS 3 : utilisé</li>
                <li className="item-js"> <FaJsSquare /> JavaScript : utilisé</li>
                <li className="item-ts"> <SiTypescript /> TypeScript : utilisé</li>
                <li className="item-php"> <SiPhp /> PHP : utilisé</li>
                <li className="item-mysql"> <SiMysql /> MySQL : utilisé</li>
                <li className="item-c-plus-plus"> <FaCuttlefish /> C++ : utilisé</li>
            </ul>
        </div>

        <div className="framework">
            <h2>Framework :</h2>
            <ul className="framework-list">
                <li className="item-symfony"> <FaSymfony /> Symfony : utilisé</li>
                <li className="item-wordpress"> <FaWordpress /> WordPress : utilisé</li>
                <li className="item-react"> <FaReact /> React : utilisé</li>
                <li className="item-angular"> <FaAngular /> Angular : vue</li>
                <li className="item-cordova"> <SiApachecordova /> Cordova : vue</li>
                <li className="item-laravel"> <SiLaravel /> Laravel : utilisé</li>
            </ul>
        </div>

        <div className="other">
            <h2>Other :</h2>
            <ul className="other-list">
                <li className="item-lando"> <BiPlanet /> Lando : utilisé</li>
                <li className="item-docker"> <FaDocker /> Docker : utilisé</li>
                <li className="item-gitlab"> <AiOutlineGitlab /> Gitlab : utilisé</li>
                <li className="item-github"> <AiFillGithub /> Github : utilisé</li>
                <li className="item-heroku"> <SiHeroku /> Heroku : utilisé</li>
                <li className="item-discord"> <SiDiscord /> Bot discord</li>
                <li className="item-ubuntu"> <SiUbuntu /> Ubuntu</li>
                <li className="item-arduino"> <SiArduino /> Arduino : utilisé</li>
            </ul>
        </div>
    </div>
</div>
);
};

export default Competence;