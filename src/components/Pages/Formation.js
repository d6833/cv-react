import React from 'react';
import "../Sidenav"



const Formation = () => {

    return (
        <div className="right-container">
          <div className="experience-formation-container-w">
           <div className="experience-professionnel">
               <h2>Experience professionnel</h2>
               <ul className="experience-list">
                   <li>
                       AgencePointCom, Perpignan, 13/09/21 - 16/11/2021 :
                       - Stage : Developpeur web ( Wordpress , Laravel )
                   </li>
               </ul>
           </div>

            <div className="formation">
                <h2>Formation</h2>

                <ul className="formation-list">
                    <li>
                        L'idem, Le Soler:
                        - Developpeur web et web mobile
                    </li>

                    <li>
                        UPVD, License :
                        - Mathématque - Informatique
                    </li>

                    <li>
                        Lycée, Pablo Picasso :
                        - STI2D -> SIN
                    </li>
                </ul>
           </div>
          </div>
        </div>
    );
};

export default Formation;